package org.bitbucket.antonnaumov.just.spreadsheets

import org.bitbucket.antonnaumov.just.spreadsheets.model._
import org.bitbucket.antonnaumov.just.spreadsheets.Implicits._
import org.scalatest.{FlatSpecLike, Matchers}

class SpreadSheetTest extends FlatSpecLike with Matchers {
  "Spreadsheet" should "raise an exception during the computation of two recursive cells" in {
    val formula = FormulaCell("A3", List("A2"))
    val error = intercept[RecursiveCellReferenceException] {
      formula.compute(List(
        IntegerCell("A1", 20),
        FormulaCell("A2", List("A3")),
        formula
      ))
    }
    error.getMessage should equal("The recursive call detected in the formula cell [A3]")
  }

  it should "raise an exception during the computation of non-existing cell" in {
    val formula = FormulaCell("A3", List("A2"))
    val error = intercept[InvalidReferenceException] {
      formula.compute(List(
        IntegerCell("A1", 20),
        formula
      ))
    }
    error.getMessage should equal("The cell [A2] is not exists in the spreadsheet")
  }

  it should "cache formula value after computation" in {
    val formula = FormulaCell("A3", List("A1", "A2"))
    val spreadSheet: SpreadSheet = List(
      IntegerCell("A1", 20),
      IntegerCell("A2", 10),
      formula
    )
    formula.compute(spreadSheet) should equal(30)
    spreadSheet.updateCell(IntegerCell("A4", 30))
    formula.compute(spreadSheet) should equal(30)
  }

  it should "recompute formula value if the reference cell value changed" in {
    val formula = FormulaCell("A3", List("A1", "A2"))
    val spreadSheet: SpreadSheet = List(
      IntegerCell("A1", 20),
      IntegerCell("A2", 10),
      formula
    )
    formula.compute(spreadSheet) should equal(30)
    spreadSheet.updateCell(IntegerCell("A1", 40))
    formula.compute(spreadSheet) should equal(50)
  }
}

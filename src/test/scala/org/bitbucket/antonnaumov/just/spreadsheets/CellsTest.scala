package org.bitbucket.antonnaumov.just.spreadsheets

import org.bitbucket.antonnaumov.just.spreadsheets.model._
import org.bitbucket.antonnaumov.just.spreadsheets.Implicits._
import org.scalatest.{FlatSpecLike, Matchers}

class CellsTest extends FlatSpecLike with Matchers {
  "FormulaCell" should "compute value of two integer cells" in {
    val formula = FormulaCell("A3", List("A1", "A2"))
    formula.compute(List(
      IntegerCell("A1", 10),
      IntegerCell("A2", 20),
      formula
    )) should equal(30)
  }
}

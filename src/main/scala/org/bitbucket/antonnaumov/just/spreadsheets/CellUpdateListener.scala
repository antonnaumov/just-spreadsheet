package org.bitbucket.antonnaumov.just.spreadsheets

import org.bitbucket.antonnaumov.just.spreadsheets.model.Ref

trait CellUpdateListener {
  def updated(position: Ref, value: Int): Unit
}

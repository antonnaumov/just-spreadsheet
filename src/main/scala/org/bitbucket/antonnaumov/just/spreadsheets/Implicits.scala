package org.bitbucket.antonnaumov.just.spreadsheets

import org.bitbucket.antonnaumov.just.spreadsheets.model.{Cell, Ref, SpreadSheet}

import scala.collection.mutable
import scala.language.implicitConversions

object Implicits {
  implicit def name2Ref(name: String): Ref = Ref(name)

  implicit def cellList2Spreadsheet(cells: List[Cell]): SpreadSheet = {
    SpreadSheet(mutable.HashMap(cells.map(c => c.position -> c): _*))
  }
}

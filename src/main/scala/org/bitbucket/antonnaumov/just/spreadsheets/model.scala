package org.bitbucket.antonnaumov.just.spreadsheets

import scala.collection.mutable

object model {

  trait Cell {
    val position: Ref

    protected var computed: Option[Int] = None
    var computationReferences: mutable.HashSet[Ref] = mutable.HashSet.empty

    final def compute(spreadSheet: SpreadSheet, path: List[Ref] = Nil): Int = {
      computed.getOrElse(computeValue(spreadSheet, path))
    }

    final def recompute(spreadSheet: SpreadSheet, path: List[Ref] = Nil): Int = {
      computed = None
      compute(spreadSheet, path)
    }

    protected def computeValue(spreadSheet: SpreadSheet, path: List[Ref]): Int
  }

  case class IntegerCell(position: Ref, value: Int) extends Cell {
    computed = Some(value)

    override protected def computeValue(spreadSheet: SpreadSheet, path: List[Ref]): Int = value
  }

  case class FormulaCell(position: Ref, references: List[Ref]) extends Cell {
    override protected def computeValue(spreadSheet: SpreadSheet, path: List[Ref]): Int = {
      if (path.contains(position)) {
        throw RecursiveCellReferenceException(position)
      }
      val value = references.foldLeft(0) {
        case (acc, ref) =>
          spreadSheet.cells.get(ref).fold(throw InvalidReferenceException(ref)) { cell =>
            cell.computationReferences += position
            acc + cell.compute(spreadSheet, path ++ List(position))
          }
      }
      computed = Some(value)
      computationReferences ++= references
      value
    }
  }

  case class Ref(name: String)

  case class SpreadSheet(cells: mutable.HashMap[Ref, Cell]) {
    private val cellUpdateListeners: mutable.HashSet[CellUpdateListener] = mutable.HashSet.empty

    def compute(): Unit = {
      cells.foreach {
        case (ref, cell) =>
          val value = cell.compute(this)
          cellUpdateListeners.foreach(_.updated(ref, value))
      }
    }

    def updateCell(cell: Cell): Unit = {
      cells.get(cell.position).fold(cells.update(cell.position, cell)) { original =>
        cell.computationReferences ++= original.computationReferences
        cells.update(cell.position, cell)
        cell.computationReferences.foreach { ref =>
          val newValue = cells(ref).recompute(this)
          cellUpdateListeners.foreach(_.updated(ref, newValue))
        }
      }
    }

    def addCellUpdateListener(listener: CellUpdateListener): Unit = {
      cellUpdateListeners += listener
    }

    def removeCellUpdateListener(listener: CellUpdateListener): Unit = {
      cellUpdateListeners -= listener
    }
  }

  case class RecursiveCellReferenceException(position: Ref) extends Exception(s"The recursive call detected in the formula cell [${position.name}]")

  case class InvalidReferenceException(position: Ref) extends Exception(s"The cell [${position.name}] is not exists in the spreadsheet")

}

lazy val root = (project in file(".")).
	settings(
		organization 														:= "org.bitbucket.antonnaumov",
		name																		:= "just-spreadsheets",
		
		scalaVersion 				in Global						:= "2.12.6",
		sourcesInBase														:= false,

		javacOptions														++= Seq("-Xmx1g", "-Xss16m"),
		scalacOptions 													++= Seq(
			"-unchecked",
			"-feature",
			"-deprecation",
			"-unchecked",
			"-explaintypes",
			"-Xlint:_",
			"-Yno-adapted-args",
			"-Ywarn-dead-code",
			"-Ywarn-numeric-widen",
			"-encoding", "UTF-8"
		),

		fork 								in Test 						:= false,
		parallelExecution 	in Test 						:= false,
    testOptions         in Test             ++= Seq(
      Tests.Argument(TestFrameworks.ScalaTest, "-u", "target/junit-reports")
    )
	)

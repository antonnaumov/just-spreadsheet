val scalaTestVersion  = "3.0.5"
val scoptVersion      = "3.7.0"

resolvers             ++= Seq(
  Resolver.mavenLocal
)

libraryDependencies   ++= Seq(
  "com.github.scopt"  %% "scopt"      % scoptVersion,

  "org.scalatest"     %% "scalatest"  % scalaTestVersion % "test"
)